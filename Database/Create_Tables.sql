create table Dim_Categories 
(
	CatId		INT IDENTITY(1,1) PRIMARY KEY,
	Category	VARCHAR(50), 
);

create table Dim_Countries 
(
	CoutryId	INT IDENTITY(1,1) PRIMARY KEY,
	country		VARCHAR(50), 
);

create table Dim_Cities 
(
	CityId		INT IDENTITY(1,1) PRIMARY KEY,
	CountryId	INT FOREIGN KEY REFERENCES Countries(CoutryId),
	City		VARCHAR(33),
);

create table Dim_Custmers
(
	CustId		INT IDENTITY(1,1) PRIMARY KEY,
	CityId		INT FOREIGN KEY REFERENCES Cities(CityId),
	Surname		VARCHAR(15),
	Name		Varchar(15),
);


create table Dim_Selesmen
(
	SelesmanId		INT IDENTITY(1,1) PRIMARY KEY,
	Surname			VARCHAR(15),
	Name			Varchar(15),
	EmpDate			DATETIME,
	BossId			INT,
);

create table Dim_Company
(
	CompanyId		INT IDENTITY(1,1) PRIMARY KEY,
	CityId			INT FOREIGN KEY REFERENCES Cities(CityId),
	Company			VARCHAR(33),
);

create table Dim_Cars
(
	CarId				INT IDENTITY(1,1) PRIMARY KEY,
	CategoryId			INT FOREIGN KEY REFERENCES Categories(CatId),
	CompanyId			INT FOREIGN KEY REFERENCES Company(CompanyId),
	Car					VARCHAR(50),
	Model				VARCHAR(50),
);


create table Transactions_facts_table
(
	CarId				INT FOREIGN KEY REFERENCES Cars(CarId),
	CustmerId			INT FOREIGN KEY REFERENCES Custmers(CustId),
	SalesmanId			INT FOREIGN KEY REFERENCES Selesmen(SelesmanId),
	SalesDate			DATETIME,
	Price				MONEY,
	Amout				INT,
	Value				MONEY,
);














































